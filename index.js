const express = require('express')
const app = express()
var port = process.env.PORT || 8080;

app.use (express.urlencoded({extended: false}))
const cors = require('cors')//New for microservice
app.use(cors())//New for microservice


app.listen(port, () =>  
    console. log(`Express server for microservice2 is running on port:${port}`))

app.get('/', (req, res) => {
    res.send("Microservice2 by Nikhil Patil");
})

app.get("/get_date_difference", (req, res) => {
    var start_date = Date.parse(req.query.startDate);
    var end_date = Date.parse(req.query.endDate);

    var diffDays = parseInt(end_date - start_date) / (1000*60*60*24);
    var resObj = {
    days: `${diffDays}` + `days`,
    weeks: `or ${Number(diffDays/7).toFixed(1)}` + `weeks`, }
        res.send(resObj);
})